| URI                      | Opération   | MIME                                                         | Requête         | Réponse                                                              |

| :----------------------- | :---------- | :---------------------------------------------               | :--             | :----------------------------------------------------                |

| /pizzas                  | GET         | <-application/json<br><-application/xml                      |                 | liste des pizzas                                                 |

| /pizzas     /{id}        | GET         | <-application/json<br><-application/xml                      |                 | une pizza  ou 404                                                |

| /pizzas     /{id}/name   | GET         | <-text/plain                                                 |                 | le nom de la pizza ou 404                                            |

| /pizzas                  | POST        | <-/->application/json<br>->application/x-www-form-urlencoded | Pizza      | Nouvelle pizza <br>409 si l'ingrédient existe déjà (même nom)    |

| /pizzas     /{id}        | DELETE      |                                                              |                 |                                                                      |

Une pizza comporte uniquement un identifiant et un nom. Sa
représentation JSON  prendra donc la forme suivante :

~~~

{
	"id": "0474e9f7-c634-4b6c-bbf1-0be862327a70	",
	"name": "Reine",
	[
		{
  			"id": "3fcc9166-7033-45b8-b3a4-0a075564ede8",
			"name": "champignons de paris"
		},
		{
  			"id": "310f090f-65d5-4db9-a038-df5045734f66",
			"name": "jambon blanc"
		},
		{
  			"id": "7f9bf936-076f-4248-a8d6-528cb51f8849",
			"name": "gruyère rapé"
		}
	]
}
~~~